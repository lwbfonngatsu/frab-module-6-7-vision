from FRA321Utils import ImageUtilities as ImUtil


def main():
    bag_type = 0
    pattern = 0
    while bag_type == 0:
        bag_type = int(input("Bag type: "))
    while pattern == 0:
        pattern = int(input("Pattern: "))

    supreme = ImUtil(bag_type, pattern, test=False)
    supreme.camera_activate()


if __name__ == "__main__":
    main()
