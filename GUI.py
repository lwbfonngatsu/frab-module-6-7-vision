import tkinter as tk
from tkinter import *
from FRA321Utils import ImageUtilities as utils


def activate():
    p = pattern.get()
    t = types.get()
    test_mode = test.get()
    if test_mode == "True":
        mode = True
    else:
        mode = False
    print("Activating with these option.\nPattern: {0}\nType: {1}\nMode: {2}"
          .format(p, t, "Test" if mode else "Real"))
    supreme = utils(int(t), int(p), test=mode)
    supreme.camera_activate()


root = tk.Tk()
frame = tk.Frame(root)
frame.pack()

pattern = StringVar(root)
choices1 = {1, 2, 3}
pattern.set(1)

types = StringVar(root)
choices2 = {1, 2, 3}
types.set(1)

test = StringVar(root)
choices3 = {"True", "False"}
test.set("True")

popupMenu = OptionMenu(frame, pattern, *choices1)
Label(frame, text="Pattern").grid(row=1, column=1)
popupMenu.grid(row=1, column=2)

popupMenu = OptionMenu(frame, types, *choices2)
Label(frame, text="Type").grid(row=2, column=1)
popupMenu.grid(row=2, column=2)

popupMenu = OptionMenu(frame, test, *choices3)
Label(frame, text="Test Mode").grid(row=3, column=1)
popupMenu.grid(row=3, column=2)

button = tk.Button(frame,
                   text="EXIT",
                   fg="red",
                   command=quit)
button.grid(row=4, column=3)
slogan = tk.Button(frame,
                   text="Start",
                   command=activate)
slogan.grid(row=4, column=1)


def change_dropdown1(*args):
    print(pattern.get())


def change_dropdown2(*args):
    print(types.get())


def change_dropdown3(*args):
    print(test.get())


root.mainloop()
