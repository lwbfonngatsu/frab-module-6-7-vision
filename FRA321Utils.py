import cv2
import numpy as np
import math
import serial
import struct


class SerialOp:
    def __init__(self, port, baudrate):
        self.ser = serial.Serial()
        self.ser.port = port
        self.ser.baudrate = baudrate
        self.ser.timeout = 1
        self.ser.dtr, self.ser.rts = 0, 0
        self.ser.open()
        print("Connected to port:", self.ser.portstr)

    def send_data(self, x1, y1, x2, y2, angle1, angle2, pattern):
        x_1 = int(y1)
        y_1 = int(x1)
        x_2, y_2 = 0, 0
        print((x_1, y_1), (x_2, y_2), (int(angle1)), angle2, pattern)
        pox_x1 = struct.pack('h', x_1)
        pox_y1 = struct.pack('h', y_1)
        byte_pox_x_11, byte_pox_x_12 = struct.unpack('>BB', pox_x1)
        byte_pox_y_11, byte_pox_y_12 = struct.unpack('>BB', pox_y1)
        pox_x2 = struct.pack('h', x_2)
        pox_y2 = struct.pack('h', y_2)
        byte_pox_x_21, byte_pox_x_22 = struct.unpack('>BB', pox_x2)
        byte_pox_y_21, byte_pox_y_22 = struct.unpack('>BB', pox_y2)
        data_out = [255, 255, byte_pox_x_11, byte_pox_x_12, byte_pox_y_11, byte_pox_y_12,
                    byte_pox_x_21, byte_pox_x_22, byte_pox_y_21, byte_pox_y_22, int(angle1), int(angle2), pattern]
        array = bytes(data_out)
        self.ser.write(array)


def unit_converter(number):
    return number / 0.7725


def find_distance2points(point1, point2):
    y1, x1 = point1
    y2, x2 = point2
    return int(((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5)


def find_angle(rect):
    """
    :param rect: [numpy array(array-like)]
                 A rectangle 4 points coordinate in pixel unit
    :return:
    """
    point1, point2, point3, point4 = sorted(rect)
    dis12 = int(find_distance2points(point1[0], point2[0]))
    dis13 = int(find_distance2points(point1[0], point3[0]))
    dis14 = int(find_distance2points(point1[0], point4[0]))
    dis23 = int(find_distance2points(point2[0], point3[0]))
    dis24 = int(find_distance2points(point2[0], point4[0]))
    dis34 = int(find_distance2points(point3[0], point4[0]))
    min_dis = min([dis12, dis13, dis14, dis23, dis24, dis34])
    if dis12 == min_dis:
        y1, x1 = point1[0]
        y2, x2 = point2[0]
    elif dis13 == min_dis:
        y1, x1 = point1[0]
        y2, x2 = point3[0]
    elif dis14 == min_dis:
        y1, x1 = point1[0]
        y2, x2 = point4[0]
    elif dis23 == min_dis:
        y1, x1 = point2[0]
        y2, x2 = point3[0]
    elif dis24 == min_dis:
        y1, x1 = point2[0]
        y2, x2 = point4[0]
    else:
        y1, x1 = point3[0]
        y2, x2 = point4[0]
    if y2 != y1:
        m2 = (x2 - x1) / (y2 - y1)
        angle = math.atan(m2)
        return math.degrees(angle)
    else:
        angle = None
        return angle


def find_objects_center(rect_points):
    vertical_lines = []
    horizontal_lines = []
    for rect in rect_points:
        rect = sorted(rect.tolist())
        (tl, tr, bl, br) = rect
        dis1 = find_distance2points(tl[0], tr[0])
        dis2 = find_distance2points(tl[0], bl[0])
        dis3 = find_distance2points(tl[0], br[0])
        if dis1 > dis2 and dis1 > dis3:
            y1, x1 = tl[0]
            y2, x2 = tr[0]
        elif dis2 > dis1 and dis2 > dis3:
            y1, x1 = tl[0]
            y2, x2 = bl[0]
        else:
            y1, x1 = tl[0]
            y2, x2 = br[0]
        vertical_line = (y2 + y1) / 2
        vertical_lines.append(vertical_line)
        horizontal_line = (x2 + x1) / 2
        horizontal_lines.append(horizontal_line)
    vertical_horizontal = zip(vertical_lines, horizontal_lines)
    return list(vertical_horizontal)


def find_world_coordinate(point):
    image_y, image_x = point
    world_x, world_y = image_x * 1.412, image_y / 0.9275
    return world_x, world_y


def check_rect(rect):
    p1, p2, p3, p4 = rect
    p1, p2, p3, p4 = p1[0], p2[0], p3[0], p4[0]
    x1, x2, x3, x4 = p1[0], p2[0], p3[0], p4[0]
    counter = 1
    if x2 - x1 < 20:
        counter += 1
    if x3 - x1 < 20:
        counter += 1
    if x4 - x1 < 20:
        counter += 1
    if counter > 2:
        return False
    else:
        return True


class ImageUtilities:
    def __init__(self, stype=1, spattern=1, test=False):
        self.spec_type = stype
        self.spec_pattern = spattern
        self.test = test
        self.gripper_status = 0

        self.counter = 0
        self.morph = 5
        self.canny = 150
        self.frame = None
        self.origin_frame = None

    def pattern_matching(self, bag_x, bag_loc):
        """
        :param bag_x: bag locate in x axis
        :param bag_loc: (y, y+h, x, x+w)
        :return:
        """
        tl, tr, br, bl = bag_loc[0].tolist()
        tl, tr, br, bl = tl[0], tr[0], br[0], bl[0]
        rect = np.array([tl, tr, br, bl], dtype=np.float32)
        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))
        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))
        dst = np.array([
            [0, 0],
            [maxWidth - 1, 0],
            [maxWidth - 1, maxHeight - 1],
            [0, maxHeight - 1]], dtype="float32")

        M = cv2.getPerspectiveTransform(rect, dst)
        warped = cv2.warpPerspective(self.origin_frame, M, (maxWidth, maxHeight))
        obj = cv2.cvtColor(warped, cv2.COLOR_BGR2HSV)
        obj2 = cv2.cvtColor(obj[20:, :], cv2.COLOR_BGR2GRAY)
        lower_red = np.array([90, 0, 0])
        upper_red = np.array([120, 255, 255])
        mask = cv2.inRange(obj, lower_red, upper_red)
        mask_m = mask.tolist()
        density = 0
        for m in mask_m:
            density += sum(m)
        density = int(density / 255)
        b = 0
        for x in obj:
            for pixel in x:
                if 160 < pixel[0] < 200:
                    b += 1
        if b > 400:
            predict = 3
        else:
            blurred = cv2.bilateralFilter(obj2, 5, 40, 60)
            can = cv2.Canny(blurred, 3, 100)
            kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11))
            morph = cv2.dilate(can, kernel)
            intensity = int((np.sum(morph == 255) / np.size(morph)) * 100)
            print("intensity:", intensity)
            if intensity > 40 or (120 >= bag_x > 270 and density > 3000) or (bag_x < 120 and density > 2800):
                predict = 2
            else:
                predict = 1
        print("Predict:", predict)
        return predict

    def rectangle_detector(self):
        """
        :return:    img: [numpy array(array-like)]
                        Image frame with already draw boundary boxes if system can detect rectangle(s).
                    return_approx: [list]
                        Approximated points of objects.
        """
        gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
        # Shadow removal
        dilated_img = cv2.dilate(gray, np.ones((2, 2), np.uint8))
        bg_img = cv2.medianBlur(dilated_img, 19)
        diff_img = 255 - cv2.absdiff(gray, bg_img)
        norm_img = diff_img.copy()
        cv2.normalize(diff_img, norm_img, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
        _, thr_img = cv2.threshold(norm_img, 230, 0, cv2.THRESH_TRUNC)
        cv2.normalize(thr_img, thr_img, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
        kernel = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])
        sharpen = cv2.filter2D(thr_img, -1, kernel)
        smoothed = cv2.bilateralFilter(sharpen, 3, 20, 60)
        edges = cv2.Canny(smoothed, self.morph, self.canny)
        undetect_area = np.zeros((210, 380))
        x_offset = y_offset = 0
        edges[y_offset:y_offset + undetect_area.shape[0], x_offset:x_offset + undetect_area.shape[1]] = undetect_area
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (self.morph, self.morph))
        closed = cv2.morphologyEx(edges, cv2.MORPH_CLOSE, kernel)
        # cv2.imshow("closed", closed)
        _, contours, _ = cv2.findContours(closed, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        return_approx = []
        for cont in contours:
            if cv2.contourArea(cont) > 6500:
                arc_len = cv2.arcLength(cont, True)
                approx = cv2.approxPolyDP(cont, 0.1 * arc_len, True)
                if len(approx) == 4:
                    return_approx.append(approx)
                    self.counter += 1
                else:
                    pass
        if len(return_approx) >= 1:
            return self.frame, return_approx
        else:
            return self.frame, None

    def camera_activate(self):
        dropped_bag = 0
        if not self.test:
            ser = SerialOp("COM14", 9600)
        cam = cv2.VideoCapture(0)
        cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
        cam.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
        while cam.isOpened():
            b_x, b_y, h, w = -1000, -1000, -1000, -1000
            _, self.frame = cam.read()
            self.frame = self.frame[80: self.frame.shape[0] - 40, 180: self.frame.shape[1] - 80]
            self.origin_frame = self.frame.copy()
            sent = False
            fr, rect = self.rectangle_detector()
            if np.any(rect) is not None:
                copy_rect = sorted(rect[0].tolist())
                if check_rect(copy_rect):
                    num = 1
                    resting = False
                    bag_angle, box_angle = 0, 0
                    try:
                        for r in rect:
                            num += 1
                        if num > 1:
                            x_s = []
                            y_s = []
                            for points in rect:
                                for point in points:
                                    y_s.append(point.tolist()[0][0])
                                    x_s.append(point.tolist()[0][1])
                            b_y, b_x = min(y_s), min(x_s)
                            h, w = max(y_s) - min(y_s), max(x_s) - min(x_s)
                            cv2.rectangle(self.frame, (b_y, b_x), (b_y + h, b_x + w), (0, 0, 255), 2)

                    except ValueError as e:
                        print(e)
                    centers = find_objects_center(rect)
                    for center, r in zip(centers, rect):
                        x = int(center[0])
                        y = int(center[1])
                        cv2.drawMarker(self.frame, (x, y), (0, 0, 255))
                        cv2.drawContours(self.frame, [r], -1, (255, 0, 0), 2)
                    if (len(centers)) == 1:
                        x_bag = int(centers[0][0])
                        y_bag = int(centers[0][1])
                        pred = self.pattern_matching(x_bag, rect)
                        bag_angle = find_angle(rect[0].tolist())
                        if bag_angle is not None:
                            if bag_angle < 0:
                                bag_angle += 180
                        if self.spec_type == pred and bag_angle is not None:
                            pass
                            # print("Bag number", dropped_bag)
                            print("Going to box.")
                        else:
                            resting = True
                            print("Going to rest place.")
                        x_bag, y_bag = find_world_coordinate((x_bag, y_bag))
                        if not self.test and bag_angle is not None:
                            print("Sending...")
                            if not resting:
                                ser.send_data(x_bag, y_bag, 0, 0, bag_angle, box_angle, self.spec_pattern)
                                dropped_bag += 1
                            else:
                                ser.send_data(x_bag, y_bag, 0, 0, bag_angle, box_angle, 4)
                            sent = True
                        elif self.test and bag_angle is not None:
                            print(int(x_bag), int(y_bag), (int(bag_angle)), box_angle, self.spec_pattern)
                            sent = True
            cv2.imshow("Video Capture", self.frame)
            if sent:
                cv2.waitKey(0)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break


if __name__ == "__main__":
    fei_utils = ImageUtilities()
    fei_utils.camera_activate()
